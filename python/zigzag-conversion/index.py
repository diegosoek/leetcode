class Solution:
    def convert(self, s: str, numRows: int) -> str:
        index = 0
        descending = True
        my_matrix = [[]]
        my_matrix = [[0 for x in range(1)] for y in range(numRows)] 
        for char in s:
            my_matrix[index].append(char)
            if numRows == 1:
                continue
            else:
                if descending:
                    if index + 1 == numRows:
                        descending = False
                        index = index - 1
                    else:
                        index = index + 1
                else:
                    if index - 1 == -1:
                        descending = True
                        index = index + 1
                    else:
                        index = index - 1

        result = ""

        for x in range(len(my_matrix)):
            for y in range(len(my_matrix[x])):
                if y != 0:
                    result = result + my_matrix[x][y]
        
        return result