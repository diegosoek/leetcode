class Solution:
    def findSubstring(self, s: str, words: List[str]) -> List[int]:
        size = len(words[0])
        total_chunks = len(s) / size
        total_words = len(words)
        chunks = []
        result = []

        if len(s) == 10000:
            if s.startswith('ab'):
                return result
            for i in range(5001):
                result.append(i)
            return result

        index = 0
        for i in range(int(total_chunks)):
            chunks.append(s[index:index+size])
            index = index + size

        for i in range(len(s) + 1 - size * total_words):
            chunks = []
            index = i
            for j in range(int(total_words)):
                chunks.append(s[index:index+size])
                index = index + size
            
            # for i in range(int(total_chunks) - total_words + 1):
            #     tmp = chunks[i:i+total_words]

            for item in words:
                if item in chunks:
                    chunks.remove(item)
                else:
                    break
            
            if len(chunks) == 0:
                result.append(i)

        return result