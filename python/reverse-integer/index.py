class Solution:
    def reverse(self, x: int) -> int:
        if x < -2147483648 or x > 2147483648:
            return 0
        str_number = str(x)
        has_negative = False
        if (str_number[0] == '-'):
            str_number = str_number[1:]
            has_negative = True
        new_str = str_number[::-1]

        new_int = 0
        if (has_negative):
            new_int = int("-" + new_str)
        else:
            new_int = int(new_str)
        
        if new_int < -2147483648 or new_int > 2147483648:
            return 0
        else:
            return new_int