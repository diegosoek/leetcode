class Solution:
    def myAtoi(self, s: str) -> int:
        number = 0
        signal = None
        s = s.strip()

        if len(s) == 0:
            return 0

        if s[0] == "+" or s[0] == "-":
            signal = s[0:1]
            s = s[1:]
        
        all_groups = re.search(r'^\d+', s)
        if all_groups != None:
            if signal != None:
                number = int(signal + all_groups.group(0))
            else:
                number = int(all_groups.group(0))
        
        if number < -2147483648:
            number = -2147483648
        if number > 2147483647:
            number = 2147483647

        return number