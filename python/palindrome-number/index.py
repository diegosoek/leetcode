import math

class Solution:
    def isPalindrome(self, x: int) -> bool:
        my_str = str(x)
        my_len = len(my_str)

        medium = math.floor(my_len / 2)

        if my_len % 2 != 0:
            if my_str[:medium] == my_str[medium+1:][::-1]:
                return True
            else:
                return False
        else:
            if my_str[:medium] == my_str[medium:][::-1]:
                return True
            else:
                return False
