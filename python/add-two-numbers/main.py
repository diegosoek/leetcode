# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        number1 = ""
        number2 = ""

        item = l1
        while item is not None:
            number1 = str(item.val) + number1
            item = item.next
        
        item = l2
        while item is not None:
            number2 = str(item.val) + number2
            item = item.next

        total = int(number1) + int(number2)
        
        tmp = list(str(total))

        result = None
        next = None
        for i in tmp:
            result = ListNode(val=int(i), next=next)
            next = result

        return result
