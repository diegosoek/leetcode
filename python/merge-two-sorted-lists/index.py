# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
class Solution:
    def mergeTwoLists(self, list1: Optional[ListNode], list2: Optional[ListNode]) -> Optional[ListNode]:
        if list1 is None and list2 is None:
            return None
        
        new_list = None
        merged = []
        
        while (True):
            if list1 is None:
                break
            merged.append(list1.val)
            list1 = list1.next

        while (True):
            if list2 is None:
                break
            merged.append(list2.val)
            list2 = list2.next

        merged.sort(reverse=True)

        for i in merged:
            if new_list is None:
                new_list = ListNode(val=i)
            else:
                new_list = ListNode(val=i, next=new_list)

        return new_list
            