# https://leetcode.com/problems/product-sales-analysis-i/?envType=study-plan-v2&envId=top-sql-50
# Write your MySQL query statement below
SELECT product_name, year, price FROM Product
INNER JOIN Sales ON Product.product_id = Sales.product_id;