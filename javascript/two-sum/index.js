/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function (nums, target) {
  let firstIdx = -1;
  let secondIdx = -1;
  nums.forEach((num, index) => {
    nums.forEach((num2, index2) => {
      if (index == index2) return;
      if (num + num2 == target) {
        firstIdx = index;
        secondIdx = index2;
        return;
      }
    });
  });
  return [firstIdx, secondIdx];
};
