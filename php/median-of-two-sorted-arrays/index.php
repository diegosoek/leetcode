<?php
class Solution {

  /**
   * @param Integer[] $nums1
   * @param Integer[] $nums2
   * @return Float
   */
  function findMedianSortedArrays($nums1, $nums2) {
      $merged = array_merge($nums1, $nums2);
      sort($merged);
      return $this->calculateMedian($merged);
  }

  function calculateMedian($array) {
      if (empty($array)) {
          return null;
      } else {
          $lowMiddle = $array[floor((count($array) - 1) / 2)];
          $highMiddle = $array[ceil((count($array) - 1) / 2)];
          return ($lowMiddle + $highMiddle) / 2;
      }
  }
}