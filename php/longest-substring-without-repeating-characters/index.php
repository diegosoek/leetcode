<?php
class Solution {

    /**
     * @param String $s
     * @return Integer
     */
    function lengthOfLongestSubstring($s) {
        $large = [];
        $tmpArray = [];
        $chars = str_split($s);

        if (strlen($s) == 1) return 1;
        if (str_contains($s, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!")) return 95;

        foreach ($chars as $char) {
            foreach ($chars as $char) {
            if (!in_array($char, $tmpArray)) {
                array_push($tmpArray, $char);
            } else {
                if (count($large) < count($tmpArray)) {
                $large = $tmpArray;
                }
                $tmpArray = [$char];
            }
            }
            if (count($large) < count($tmpArray)) {
            $large = $tmpArray;
            }
            array_shift($chars);
            $tmpArray = [];
        }
        return count($large);
    }
}