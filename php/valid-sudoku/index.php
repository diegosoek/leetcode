<?php
class Solution {

    /**
     * @param String[][] $board
     * @return Boolean
     */
    function isValidSudoku($board) {
        if (count($board) != 9) return false;
        if (count($board[0]) != 9) return false;

        // Valid lines
        foreach ($board as $line) {
            foreach(array_count_values($line) as $val => $c){
                if($val != '.' && $val != ',' && $c > 1) return false;
            }
        }

        // Valid columns
        for ($i = 0;$i < count($board);$i++){
            $tmp = [];
            for ($j = 0;$j < count($board[$i]);$j++){
                array_push($tmp, $board[$j][$i]);
            }
            var_dump($tmp);
            foreach(array_count_values($tmp) as $val => $c){
                if($val != '.' && $val != ',' && $c > 1) return false;
            }
        }

        // Valid squares
        for ($i = 0;$i < count($board);$i+=3){
            for ($j = 0;$j < count($board);$j+=3){
                $tmp = [];
                for ($k = $i;$k < $i+3;$k++){
                    for ($l = $j;$l < $j+3;$l++){
                        array_push($tmp, $board[$k][$l]);
                    }
                }
                foreach(array_count_values($tmp) as $val => $c){
                    if($val != '.' && $val != ',' && $c > 1) return false;
                }
            }
        }
        return true;
    }
}